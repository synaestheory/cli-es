use crate::{ApiCommand, ApiResult, ESClient};
use async_trait::async_trait;
use serde::Serialize;
use structopt::StructOpt;

#[derive(Copy, Clone, Debug, StructOpt)]
pub struct Command {
    #[structopt(short)]
    primary_shards_only: bool,
}

#[derive(Serialize)]
struct Query {
    pri: bool,
}

impl From<Command> for Query {
    fn from(command: Command) -> Self {
        Query {
            pri: command.primary_shards_only,
        }
    }
}

impl From<bool> for Command {
    fn from(primary_shards_only: bool) -> Self {
        Command {
            primary_shards_only: primary_shards_only,
        }
    }
}

#[async_trait(?Send)]
impl ApiCommand for Command {
    async fn process(self, client: &ESClient) -> ApiResult {
        surf::get(format!("{}/_cat/indices", &client.host))
            .set_query(&Query::from(self))?
            .recv_string()
            .await
    }
}
