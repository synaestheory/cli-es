use crate::{ApiCommand, ApiResult, ESClient};
use async_trait::async_trait;
use structopt::StructOpt;
mod create;
mod delete;
mod info;
mod list;

#[derive(Clone, Debug, StructOpt)]
pub enum Command {
    Create(create::Command),
    Info(info::Command),
    Delete(delete::Command),
    List(list::Command),
}

#[derive(Clone, Debug, StructOpt)]
pub struct Request {
    #[structopt(short, long)]
    /// Display primary shards only
    primary_shards_only: bool,
    #[structopt(subcommand)]
    command: Option<Command>,
}

use Command::*;

#[async_trait(?Send)]
impl ApiCommand for Request {
    async fn process(self, client: &ESClient) -> ApiResult {
        match self.command {
            Some(action) => match action {
                Create(command) => command.process(client).await,
                Delete(command) => command.process(client).await,
                Info(command) => command.process(client).await,
                List(command) => command.process(client).await,
            },
            None => {
                list::Command::from(self.primary_shards_only)
                    .process(client)
                    .await
            }
        }
    }
}
