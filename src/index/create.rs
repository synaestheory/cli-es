use crate::{ApiCommand, ApiResult, ESClient};
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::io::BufReader;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Clone, Debug, StructOpt)]
pub struct Command {
    #[structopt(long, short)]
    /// Enables the best compression, may affect query speeds.
    best_compression: bool,
    #[structopt(about = "name of the index")]
    name: String,
    #[structopt(long, short)]
    /// The number of replicas per primary shard.
    replicas: Option<u32>,
    #[structopt(long, short)]
    /// The number of primary shards that an index should have.
    shards: Option<u32>,
    #[structopt(long, short, parse(from_os_str))]
    /// path to mappings file
    mappings_file: Option<PathBuf>,
}

#[derive(Serialize)]
struct Body {
    mappings: Option<serde_json::Value>,
    settings: IndexSettings,
}

impl From<Command> for Body {
    fn from(command: Command) -> Self {
        let settings = IndexSettings {
            codec: Codec::from(command.best_compression),
            number_of_replicas: command.replicas,
            number_of_shards: command.shards,
        };

        let mappings = command
            .mappings_file
            .map(std::fs::File::open)
            .and_then(Result::ok)
            .map(BufReader::new)
            .map(serde_json::from_reader)
            .and_then(Result::unwrap);

        Self { settings, mappings }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub enum Codec {
    Default,
    BestCompression,
}

impl From<String> for Codec {
    fn from(codec: String) -> Self {
        if codec == "best_compression" {
            Self::BestCompression
        } else {
            Self::Default
        }
    }
}

impl From<bool> for Codec {
    fn from(best_compression: bool) -> Self {
        if best_compression {
            Codec::BestCompression
        } else {
            Codec::Default
        }
    }
}

#[derive(Serialize)]
struct IndexSettings {
    codec: Codec,
    number_of_replicas: Option<u32>,
    number_of_shards: Option<u32>,
}

#[async_trait(?Send)]
impl ApiCommand for Command {
    async fn process(self, client: &ESClient) -> ApiResult {
        let url = format!("{}/{}?pretty=true", &client.host, self.name);
        surf::put(url)
            .body_json(&Body::from(self))?
            .recv_string()
            .await
    }
}
