use crate::{ApiCommand, ApiResult, ESClient};
use async_trait::async_trait;
use structopt::StructOpt;

#[derive(Clone, Debug, StructOpt)]
pub struct Command {
    #[structopt(about = "name of the index")]
    name: String,
}

#[async_trait(?Send)]
impl ApiCommand for Command {
    async fn process(self, client: &ESClient) -> ApiResult {
        surf::get(format!("{}/{}?pretty=true", &client.host, self.name))
            .recv_string()
            .await
    }
}
