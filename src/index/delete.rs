use crate::{ApiCommand, ApiResult, ESClient};
use async_trait::async_trait;
use structopt::StructOpt;

#[derive(Clone, Debug, StructOpt)]
pub struct Command {
    /// Index to delete
    name: String,
}

#[async_trait(?Send)]
impl ApiCommand for Command {
    async fn process(self, client: &ESClient) -> ApiResult {
        let confirmation_text = format!(
            "Are you sure? Delete Index: `{}` on Host: `{}`",
            self.name, &client.host
        );
        if dialoguer::Confirmation::new()
            .with_text(&confirmation_text)
            .interact()?
        {
            surf::delete(format!("{}/{}?pretty=true", &client.host, self.name))
                .recv_string()
                .await
        } else {
            Ok("Aborted".to_string())
        }
    }
}
