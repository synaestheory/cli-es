use crate::{ApiCommand, ApiResult, ESClient};
use async_trait::async_trait;
use serde::{Serialize};
use structopt::StructOpt;

#[derive(Clone, Debug, Serialize)]
struct ReindexSettings {
    index: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    version_type: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    query: Option<String>,
}

#[derive(Clone, Debug, Serialize)]
struct Script {
    source: String,
}

impl From<String> for Script {
    fn from(source: String) -> Self {
        Self { source }
    }
}

impl From<String> for ReindexSettings {
    fn from(index: String) -> Self {
        Self { index, query: None, version_type: None }
    }
}

#[derive(Clone, Debug, Serialize)]
struct Body {
    source: ReindexSettings,
    dest: ReindexSettings,
    #[serde(skip_serializing_if = "Option::is_none")]
    script: Option<Script>,
}

impl From<Request> for Body {
    fn from(request: Request) -> Self {
        let source = ReindexSettings::from(request.source);
        let dest = ReindexSettings::from(request.dest);

        Self { source, dest, script: request.script.map(Script::from) }
    }
}


#[derive(Clone, Debug, StructOpt)]
pub struct Request {
    /// Source index.
    #[structopt()]
    source: String,
    /// Destination index.
    #[structopt()]
    dest: String,
    /// Optional script to run during reindex.
    #[structopt(short, long)]
    script: Option<String>,
}

#[async_trait(?Send)]
impl ApiCommand for Request {
    async fn process(self, client: &ESClient) -> ApiResult {
        let url = format!("{}/_reindex?pretty=true", &client.host);
        surf::post(url)
            .body_json(&Body::from(self))?
            .recv_string()
            .await
    }
}
