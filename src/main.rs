//! `cli-es` is a cli interface for ElasticSearch.
//!
//! It is still early in development.
//!
//! Only a few features have been implemented and the API is subject to change.
//!
//! Currently you can create/delete and list indices, you can create snapshots repositories and create snapshots.
//!
//! # Examples
//! ```
//! cli-es --help
//! ```
//! By default, `cli-es` will try to connect to `http://localhost:9200`.
//! If you want to hit a different host, you can either use the `ES_HOST` environment variable, or pass in a `-h` or `--host` argument to `cli-es`.
//! ```
//! cli-es
//! ES_HOST=http://elasticsearch:9200 cli-es
//! cli-es -h http://es:9200 index --help
//! ```
//! Create an index named books with 5 primary shards and 3 replicas
//! ```
//! cli-es index create books --shards 5 --replicas 3
//! ```
//! Get information about the books index
//! ```
//! cli-es index books
//! ```
//! Create snapshot repository (Must be done before creating a snapshot)
//! ```
//! cli-es snapshot repository create /var/backups my_backups
//! ```
//! Create snapshot
//! ```
//! cli-es snapshot create my_backups newest_backup_1 --wait-for-completion

use futures::executor::block_on;
use structopt::StructOpt;
mod index;
mod snapshot;
mod reindex;

use async_trait::async_trait;

#[async_trait(?Send)]
pub trait ApiCommand {
    async fn process(self, client: &ESClient) -> ApiResult;
}

#[derive(Debug, StructOpt)]
#[structopt(about = "An Elasticsearch Client for your CLI")]
pub struct ESClient {
    #[structopt(
        long = "host",
        short = "h",
        default_value = "http://localhost:9200",
        env = "ES_HOST"
    )]
    /// Elasticsearch host
    host: String,
    #[structopt(subcommand)]
    api: Option<API>,
}

#[derive(Clone, Debug, StructOpt)]
pub enum API {
    #[structopt(about = "Manage indices on your elasticsearch instance")]
    Index(index::Request),
    #[structopt(about = "Manage snapshots")]
    Snapshot(snapshot::Request),
    #[structopt(about = "Reindex documents from one index to another")]
    Reindex(reindex::Request),
}

pub type ApiResult = Result<String, Box<dyn std::error::Error + Send + Sync>>;

impl ESClient {
    async fn run(&self) {
        let result = match &self.api {
            Some(api) => match api {
                API::Index(request) => request.to_owned().process(self).await,
                API::Snapshot(request) => request.to_owned().process(self).await,
                API::Reindex(request) => request.to_owned().process(self).await,
            },
            None => surf::get(format!("{}", &self.host)).recv_string().await,
        };
        handle_result(&result).await
    }
}

async fn handle_result(result: &ApiResult) {
    match result {
        Ok(text) => println!("{}", text),
        Err(err) => println!("{:?}", err),
    };
}

fn main() {
    let client = ESClient::from_args();
    block_on(client.run());
}
