use crate::{ApiResult, ESClient};
use serde::{Deserialize, Serialize};
use structopt::StructOpt;

#[derive(Clone, Debug, StructOpt)]
pub struct Command {
    /// fs location for the repository
    repository: String,
    /// name of the repository
    name: String,
    #[structopt(short, long)]
    wait_for_completion: bool,
}

#[derive(Deserialize, Serialize)]
struct Query {
    pretty: bool,
    wait_for_completion: bool,
}

#[derive(Deserialize, Serialize)]
struct Body {}

impl From<Command> for Query {
    fn from(command: Command) -> Self {
        Self {
            pretty: true,
            wait_for_completion: command.wait_for_completion,
        }
    }
}

#[derive(Deserialize, Serialize)]
struct RepositorySettings {
    location: String,
}

pub async fn run(client: &ESClient, command: Command) -> ApiResult {
    let url = format!(
        "{}/_snapshot/{}/{}",
        &client.host, command.repository, command.name
    );
    surf::put(url)
        .body_json(&serde_json::json!({}))?
        .set_query(&Query::from(command))?
        .recv_string()
        .await
}
