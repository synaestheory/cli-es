use crate::{ApiCommand, ApiResult, ESClient};
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use structopt::StructOpt;

#[derive(Clone, Debug, StructOpt)]
pub struct Command {
    /// fs location for the repository
    location: String,
    /// name of the repository
    name: String,
}

#[derive(Deserialize, Serialize)]
struct Body {
    #[serde(rename = "type")]
    repo_type: String,
    settings: RepositorySettings,
}

impl From<Command> for Body {
    fn from(Command { name: _, location }: Command) -> Self {
        let settings = RepositorySettings { location };
        let repo_type = "fs".to_string();
        Self {
            settings,
            repo_type,
        }
    }
}

#[derive(Deserialize, Serialize)]
struct RepositorySettings {
    location: String,
}

#[async_trait(?Send)]
impl ApiCommand for Command {
    async fn process(self, client: &ESClient) -> ApiResult {
        let url = format!("{}/_snapshot/{}?pretty=true", &client.host, self.name);
        surf::put(url)
            .body_json(&Body::from(self))?
            .recv_string()
            .await
    }
}
