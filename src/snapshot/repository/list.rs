use crate::{ApiCommand, ApiResult, ESClient};
use async_trait::async_trait;
use structopt::StructOpt;

#[derive(Clone, Debug, StructOpt)]
pub struct Command {
    /// Repository to list information for, wildcards are accepted and multiple entries are accepted as well
    repositories: Vec<String>,
}

impl Command {
    pub fn new() -> Self {
        Self {
            repositories: vec![],
        }
    }
}

#[async_trait(?Send)]
impl ApiCommand for Command {
    async fn process(self, client: &ESClient) -> ApiResult {
        let url = format!("{}/_snapshot/_all?pretty=true", &client.host);
        surf::get(url).recv_string().await
    }
}
