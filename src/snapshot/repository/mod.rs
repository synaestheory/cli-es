mod create;
mod list;

use crate::{ApiCommand, ApiResult, ESClient};
use async_trait::async_trait;

// use serde::{Deserialize, Serialize};
use structopt::StructOpt;
use Command::*;

#[derive(Clone, Debug, StructOpt)]
pub enum Command {
    /// Create a new snapshot repository
    Create(create::Command),
    /// list all snapshot repositories
    List(list::Command),
}

#[derive(Clone, Debug, StructOpt)]
pub struct Request {
    #[structopt(subcommand)]
    command: Option<Command>,
}

#[async_trait(?Send)]
impl ApiCommand for Request {
    async fn process(self, client: &ESClient) -> ApiResult {
        match self.command {
            Some(action) => match action {
                Create(command) => command.process(client).await,
                List(command) => command.process(client).await,
            },
            None => list::Command::new().process(client).await,
        }
    }
}
