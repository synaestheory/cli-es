mod create;
mod repository;

use crate::{ApiCommand, ApiResult, ESClient};
use async_trait::async_trait;
use structopt::StructOpt;

pub use create::run as create;

#[derive(Clone, Debug, StructOpt)]
pub enum Command {
    /// Snapshot Repository sub-commands
    Repository(repository::Request),
    /// Create a snapshot
    Create(create::Command),
}

#[derive(Clone, Debug, StructOpt)]
pub struct Request {
    #[structopt(subcommand)]
    command: Option<Command>,
}

use Command::*;

#[async_trait(?Send)]
impl ApiCommand for Request {
    async fn process(self, client: &ESClient) -> ApiResult {
        match self.command {
            Some(action) => match action {
                Repository(request) => request.process(client).await,
                Create(command) => create(client, command).await,
            },
            None => Ok("".to_string()),
        }
    }
}
