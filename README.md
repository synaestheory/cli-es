# cli-es

`cli-es` is a cli interface for ElasticSearch.

It is still early in development.

Only a few features have been implemented and the API is subject to change.

Currently you can create/delete and list indices, you can create snapshots repositories and create snapshots.

## Examples
```rust
cli-es --help
```
By default, `cli-es` will try to connect to `http://localhost:9200`.
If you want to hit a different host, you can either use the `ES_HOST` environment variable, or pass in a `-h` or `--host` argument to `cli-es`.
```rust
cli-es
ES_HOST=http://elasticsearch:9200 cli-es
cli-es -h http://es:9200 index --help
```
Create an index named books with 5 primary shards and 3 replicas
```rust
cli-es index create books --shards 5 --replicas 3
```
Get information about the books index
```rust
cli-es index books
```
Create snapshot repository (Must be done before creating a snapshot)
```rust
cli-es snapshot repository create /var/backups my_backups
```
Create snapshot
```rust
cli-es snapshot create my_backups newest_backup_1 --wait-for-completion
